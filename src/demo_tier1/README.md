# Demo Tier 1 Certificate Authority and Firmware Generation
## Generating Certificate Authority
Before creating a firmware image, you have to create the certificate authority. Running the following script will generate a default certificate authority for a "Demo Tier 1 Supplier" located in East Lansing, MI, that has an "ECU" line with one product model named "Computer_001:"
```
./tier1_gen_certs.sh
```
To generate a custom certificate authority, use the -tn flag to set the name of the Tier 1 supplier, -pt to set the product type, and -pm to set the product model.

If you already have existing certificates and simply want to make lower-level certificates, use the -kr flag and/or -kp flag to specify the path to the root/intermediate certificates:
```
./tier1_gen_certs.sh -kr keystore/private/tier1_root.key -kp keystore/private/tier1_ECU.key -pm "Computer_002"
```
## Creating a firmware image
To create the Tier 1 segment of the firmware image, use the gen_fw.sh script, specifying the path to the product model key using the -ks flag:
```
./gen_fw.sh -ks keystore/private/tier1_model_Computer_001.key
```
You will be prompted to edit a YAML file; edit the configuration to your liking. After exiting, the YAML file will be converted to Base64, signed, and compiled into a file (default: ECU\_Computer\_001.fw).

Note that this is only one half of a complete update; to make a complete update, you must also use the manufacturer firmware tools.
