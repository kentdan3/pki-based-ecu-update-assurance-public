#!/bin/sh

KEYSTORE=./keystore
PROD_TYPE=ECU
PROD_MODEL=Computer_001
PRIVATE_KEY_PATH=$KEYSTORE/private/tier1_model_$PROD_MODEL.key
OUTPUT_FILE=${PROD_TYPE}_${PROD_MODEL}.fw

print_usage()
{
  echo "gen_config_update : Generate signed firmware blob"
  echo ""
  echo "Usage: gen_config"
  echo "  -c <config> : path to config file (default: prompts for config options)"
  echo "  -ks, --private-key <keyfile> : path to private signing key (default: $KEYSTORE/private/mfr_model_$MODEL.key"
  echo "  -kp, --public-cert <certfile> : path to public X.509 cert for signing key (default: $KEYSTORE/public/mfr_model_$MODEL.crt"
  echo "  -o <path> : path to output config firmware (default: $OUTPUT_FILE)"
  
}

exit_failure()
{
if [ "$1" != "" ]; then
  echo "Error: $1"
else
  echo "Exited with unknown failure."
fi
print_usage
exit
}

while [ "$1" != "" ]; do
  case $1 in
    -h|-\?|--help)
      print_usage; exit
      ;;
    -ks)
      shift
      if [ ! -e "$1" ]; then exit_failure "$1 is not a valid keyfile."; fi
      PRIVATE_KEY_PATH="$1"
      ;;
    -o|--output)
      shift
      OUTPUT_FILE="$1"
      ;;
    *)
  esac
  shift
done

echo "message_string : ECU firmware is working normally." > .tmp_fw
echo "min_frequency : 1" >> .tmp_fw
echo -n "max_frequency : 10" >> .tmp_fw
vim .tmp_fw
echo "Converting firmware to Base64"
FW_STRING=$(cat .tmp_fw | base64 -w 0)
echo -n "$FW_STRING" > .tmp_fw_b64
echo "Signing firmware with SHA256 ($PRIVATE_KEY_PATH)"
FW_SIG=$(openssl dgst -sha256 -sign $PRIVATE_KEY_PATH .tmp_fw_b64 | base64 -w 0)
echo "Writing firmware file as $PROD_TYPE"
printf "%s\n%s\n" $FW_STRING $FW_SIG > "${OUTPUT_FILE}";
echo "Removing temp files..."
rm .tmp_fw .tmp_fw_b64
echo "Done!"

