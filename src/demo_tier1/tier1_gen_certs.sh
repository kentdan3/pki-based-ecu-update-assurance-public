#!/bin/sh

get_help ()
{
  echo "Certificate Authority Generator"
  echo "Written by Daniel Kent (kentdan3@egr.msu.edu)"
  echo ""
  echo "Options:"
  echo "  -?, -h, --help  : Show this help menu"
  echo "  -nr, --no-root : Do not generate root certificate authority keys."
  echo "  -np, --no-prod : Do not generate product certificate"
  echo "                   authority keys."
  echo "  -nm, --no-model : Do not generate specific model key."
  echo "  -kr, --root-key <private-key> <cert> : Use an existing root certificate"
  echo "                                         authority key. Implies -nr"
  echo "  -kp, --prod-key <private-key> <cert> : Use an existing product model"
  echo "                                         key. Implies -np"
  echo "  -tn, --tier1-name : Sets the Tier 1 supplier name. Default: \"Demo"
  echo "                      Tier 1 Supplier\""
  echo "  -pt, --product-type : Sets the product type. Default: ECU"
  echo "  -pm, --product-model : Sets the product model. Default: Computer_001"
}

exit_failure()
{
if [ "$1" != "" ]; then
  echo "Error: $1"
else
  echo "Exited with unknown failure."
fi
get_help
exit
}

KEYSTORE_PATH=./keystore
PROD_TYPE=ECU
PROD_MODEL=Computer_001
TIER1_NAME="Demo Tier 1 Supplier"

TIER1_CT="US"
TIER1_ST="MI"
TIER1_CITY="East Lansing"

GEN_ROOT_KEY=1
ROOT_PRIVATE_KEY_PATH=
ROOT_CERT_PATH=
GEN_PROD_KEY=1
PROD_PRIVATE_KEY_PATH=
PROD_CERT_PATH=
GEN_MODEL_KEY=1

# Parsing

while [ "$1" != "" ]; do
  case $1 in
    -h|-\?|--help)
      echo "Help requested."
      get_help
      exit
      ;;

    -nr|--no-root)
      GEN_ROOT_KEY=0
      ;;

    -np|--no-prod)
      GEN_PROD_KEY=0
      ;;
    -nm|--no-model)
      GEN_MODEL_KEY=0
      ;;

    -kr|--root-key)
      GEN_ROOT_KEY=0
      shift
      if [ ! -e $1 ]; then exit_failure "-kr requires two additional parameters."; fi;
      if [ -e $1 ]; then ROOT_PRIVATE_KEY_PATH=$1; else exit_failure "$1 is not a valid keyfile."; fi;
      shift
      if [ ! -e $1 ]; then exit_failure "-kr requires two additional parameters."; fi;
      if [ -e $1 ]; then ROOT_CERT_PATH=$1; else exit_failure "$1 is not a valid cert."; fi;
      ;;

    -kp|--prod-key)
      GEN_PROD_KEY=0
      shift
      if [ ! -e $1 ]; then exit_failure "-kp requires two additional parameters."; fi;
      if [ -e $1 ]; then PROD_PRIVATE_KEY_PATH=$1; else exit_failure "$1 is not a valid keyfile."; fi;
      shift
      if [ ! -e $1 ]; then exit_failure "-kp requires twp additional parameters."; fi;
      if [ -e $1 ]; then PROD_CERT_PATH=$1; else exit_failure "$1 is not a valid cert."; fi;
      ;;

    -i|--init)
      OUTPUT_INIT_DEMO_ECU_FW=1
    ;;

    -tn)
      shift
      if [ -z "$1" ]; then exit_failure "-tm requires an additional parameter."; fi;
      TIER1_NAME="$1"
    ;;
    -pt)
      shift
      if [ -z "$1" ]; then exit_failure "-pt requires an additional parameter."; fi;
      PROD_TYPE="$1"
    ;;
    -pm)
      shift
      if [ -z "$1" ]; then exit_failure "-pm requires an additional parameter."; fi;
      PROD_MODEL="$1"
    ;;
    *)
      exit_failure "Unrecognized option: $1"
    ;;
  esac
  shift
done

# Checking for prerequisites

OPENSSL_FOUND=$(which openssl > /dev/null; echo $?);

if [ $OPENSSL_FOUND -ne 0 ]; then
  echo "OpenSSL was not found on this system. Please install OpenSSL to use this script."
fi

# --------- Generating Tier1 Certificate Chain -----
#
#

# ---- Generating Master CA ----

if [ ! -e "$KEYSTORE_PATH" ]; then
  echo "Initializing keystore."
  mkdir -p $KEYSTORE_PATH/public $KEYSTORE_PATH/private
fi

if [ $GEN_ROOT_KEY -eq 1 ]; then
  echo "Generating Tier 1 root key."
  openssl genrsa -out $KEYSTORE_PATH/private/tier1_root.key 4096
  openssl rsa -in $KEYSTORE_PATH/private/tier1_root.key -pubout > $KEYSTORE_PATH/public/tier1_root.key.pub

  echo "Preparing signing request for Tier 1 root key."
  openssl req -new -sha256 -key $KEYSTORE_PATH/private/tier1_root.key -out $KEYSTORE_PATH/private/tier1_root.csr -subj "/C=$TIER1_CT/ST=$TIER1_ST/L=$TIER1_CITY/O=$TIER1_NAME/CN=Root Firmware Certificate Authority"

  # Comment: this certificate expires in ~30 years. In practice, vehicle manufacturers
  # will have to determine how long they want to make their firmware certificates
  # valid for. Generally, certs have much shorter expirations in order to enhance
  # security and lessen the chance that someone duplicates the key through its
  # expected lifetime.

  echo "Signing root Tier 1 key."
  openssl x509 -signkey $KEYSTORE_PATH/private/tier1_root.key -in $KEYSTORE_PATH/private/tier1_root.csr -req -days 10957 -out $KEYSTORE_PATH/public/tier1_root.pem
  ROOT_PRIVATE_KEY_PATH=$KEYSTORE_PATH/private/tier1_root.key
  ROOT_CERT_PATH=$KEYSTORE_PATH/public/tier1_root.pem
elif [ $GEN_ROOT_KEY -eq 0 ] && [ $GEN_PROD_KEY -eq 1 ]; then 
  # We only need the root key if we are generating a product type key
  openssl rsa -in $ROOT_PRIVATE_KEY_PATH -text -noout > /dev/null 2>&1
  if [ $? -eq 1 ] ; then
    exit_failure "$ROOT_PRIVATE_KEY_PATH is not a valid private key."
  fi
fi

# ---- Generating Intermediate CA ----
if [ $GEN_PROD_KEY -eq 1 ]; then

  echo "Generating Tier 1 Product Type $PROD_TYPE key."
  openssl genrsa -out $KEYSTORE_PATH/private/tier1_$PROD_TYPE.key 4096
  openssl rsa -in $KEYSTORE_PATH/private/tier1_$PROD_TYPE.key -pubout > $KEYSTORE_PATH/public/tier1_$PROD_TYPE.key.pub

  echo "Preparing signing request for $PROD_TYPE key."
  openssl req -new -sha256 -key $KEYSTORE_PATH/private/tier1_$PROD_TYPE.key -out $KEYSTORE_PATH/private/tier1_$PROD_TYPE.csr -subj "/C=$TIER1_CT/ST=$TIER1_ST/L=$TIER1_CITY/O=$TIER1_NAME/CN=$PROD_TYPE Firmware Certificate Authority"

  # Comment: again, this cert has a relatively long expiry (10 years).
  echo "Signing product type \"$PROD_TYPE\" key with root Tier 1 key"
  openssl x509 -req -in $KEYSTORE_PATH/private/tier1_$PROD_TYPE.csr -CA $ROOT_CERT_PATH -CAkey $ROOT_PRIVATE_KEY_PATH -CAcreateserial -out $KEYSTORE_PATH/public/tier1_$PROD_TYPE.crt -days 3652 -sha256
  PROD_PRIVATE_KEY_PATH=$KEYSTORE_PATH/private/tier1_$PROD_TYPE.key
  PROD_CERT_PATH=$KEYSTORE_PATH/public/tier1_$PROD_TYPE.crt
elif [ $GEN_PROD_KEY -eq 0 ] && [ $GEN_MODEL_KEY -eq 1 ]; then
  openssl rsa -in $PROD_PRIVATE_KEY_PATH -text -noout > /dev/null 2>&1
  if [ $? -eq 1 ] ; then
    exit_failure "$PROD_PRIVATE_KEY_PATH is not a valid private key."
  fi
fi

# ---- Generating Model Key ----
if [ $GEN_MODEL_KEY -eq 1 ]; then
  echo "Generating Tier 1 Model \"$PROD_MODEL\" key"
  openssl genrsa -out $KEYSTORE_PATH/private/tier1_model_$PROD_MODEL.key 4096
  openssl rsa -in $KEYSTORE_PATH/private/tier1_model_$PROD_MODEL.key -pubout > $KEYSTORE_PATH/public/tier1_model_$PROD_MODEL.key.pub

  echo "Preparing signing request for Tier 1 model \"$PROD_MODEL\" key."
  openssl req -verbose -new -sha256 -key $KEYSTORE_PATH/private/tier1_model_$PROD_MODEL.key -out $KEYSTORE_PATH/private/tier1_model_$PROD_MODEL.csr -subj "/C=$TIER1_CT/ST=$TIER1_ST/L=$TIER1_CITY/O=$TIER1_NAME/CN=$PROD_TYPE Model $PROD_MODEL Firmware Certificate Key"


  echo "Signing model \"$PROD_MODEL\" key with Tier 1 product type $PROD_TYPE key"
  # Again, 10-year validity period
  openssl x509 -req -in $KEYSTORE_PATH/private/tier1_model_$PROD_MODEL.csr -CA $PROD_CERT_PATH -CAkey $PROD_PRIVATE_KEY_PATH -CAcreateserial -out $KEYSTORE_PATH/private/tier1_model_$PROD_MODEL.crt -days 3652 -sha256
fi


