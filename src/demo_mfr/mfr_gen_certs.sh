#!/bin/sh
YEAR=$(date +%Y)

get_help ()
{
  echo "Certificate Authority Generator"
  echo "Written by Daniel Kent (kentdan3@egr.msu.edu)"
  echo ""
  echo "Options:"
  echo "  -?, -h, --help  : Show this help menu"
  echo "  -nr, --no-root : Do not generate root certificate authority keys."
  echo "  -ny, --no-year : Do not generate model year certificate"
  echo "                   authority keys."
  echo "  -nm, --no-model : Do not generate specific model key."
  echo "  -kr, --root-key <private-key> <cert> : Use an existing root certificate"
  echo "                                         authority key. Implies -nr"
  echo "  -ky, --year-key <private-key> <cert> : Use an existing product model"
  echo "                                         key. Implies -np"
  echo "  -mn, --mfr-name : Sets the manufacturer name. Default: \"Demo Manufacturer\""
  echo "  -py, --product-year : Sets the model year. Default: $YEAR"
  echo "  -pm, --product-model : Sets the product model. Default: Sparty"
}

KEYSTORE_PATH=./keystore
YEAR=$(date +%Y)
MODEL="Sparty"
MFR_NAME="Demo Manufacturer"

MFR_CT="US"
MFR_ST="MI"
MFR_CITY="East Lansing"

GEN_ROOT_KEY=1
ROOT_PRIVATE_KEY_PATH=
ROOT_CERT_PATH=
GEN_YEAR_KEY=1
YEAR_PRIVATE_KEY_PATH=
YEAR_CERT_PATH=
GEN_MODEL_KEY=1

exit_failure()
{
if [ "$1" != "" ]; then
  echo "Error: $1"
else
  echo "Exited with unknown failure."
fi
get_help
exit
}

# Parsing

while [ "$1" != "" ]; do
  case $1 in
    -h|-\?|--help)
      echo "Help requested."
      get_help
      exit
      ;;

    -nr|--no-root)
      GEN_ROOT_KEY=0
      ;;

    -ny|--no-year)
      GEN_YEAR_KEY=0
      ;;
    -nm|--no-model)
      GEN_MODEL_KEY=0
      ;;

    -kr|--root-key)
      GEN_ROOT_KEY=0
      shift
      if [ ! -e $1 ]; then exit_failure "-kr requires two additional parameters."; fi;
      if [ -e $1 ]; then ROOT_PRIVATE_KEY_PATH=$1; else exit_failure "$1 is not a valid keyfile."; fi;
      shift
      if [ ! -e $1 ]; then exit_failure "-kr requires two additional parameters."; fi;
      if [ -e $1 ]; then ROOT_CERT_PATH=$1; else exit_failure "$1 is not a valid cert."; fi;
      ;;

    -ky|--year-key)
      GEN_YEAR_KEY=0
      shift
      if [ ! -e $1 ]; then exit_failure "-ky requires two additional parameters."; fi;
      if [ -e $1 ]; then YEAR_PRIVATE_KEY_PATH=$1; else exit_failure "$1 is not a valid keyfile."; fi;
      shift
      if [ ! -e $1 ]; then exit_failure "-ky requires twp additional parameters."; fi;
      if [ -e $1 ]; then YEAR_CERT_PATH=$1; else exit_failure "$1 is not a valid cert."; fi;
      ;;

    -mn)
      shift
      if [ -z "$1" ]; then exit_failure "-tm requires an additional parameter."; fi;
      MFR_NAME="$1"
    ;;
    -py)
      shift
      if [ -z "$1" ]; then exit_failure "-py requires an additional parameter."; fi;
      YEAR="$1"
    ;;
    -pm)
      shift
      if [ -z "$1" ]; then exit_failure "-pm requires an additional parameter."; fi;
      MODEL="$1"
    ;;
    *)
      exit_failure "Unrecognized option: $1"
    ;;
  esac
  shift
done

# Checking for prerequisites

OPENSSL_FOUND=$(which openssl > /dev/null; echo $?);

if [ $OPENSSL_FOUND -ne 0 ]; then
  echo "OpenSSL was not found on this system. Please install OpenSSL to use this script."
fi

# --------- Generating Manufacturer Certificate Chain -----
#
#

# ---- Generating Master CA ----

if [ ! -e "$KEYSTORE_PATH" ]; then
  echo "Initializing keystore."
  mkdir -p $KEYSTORE_PATH/public $KEYSTORE_PATH/private
fi

if [ $GEN_ROOT_KEY -eq 1 ]; then
  echo "Generating Tier 1 root key."
  openssl genrsa -out $KEYSTORE_PATH/private/mfr_root.key 4096
  openssl rsa -in $KEYSTORE_PATH/private/mfr_root.key -pubout > $KEYSTORE_PATH/public/mfr_root.key.pub

  echo "Preparing signing request for Tier 1 root key."
  openssl req -new -sha256 -key $KEYSTORE_PATH/private/mfr_root.key -out $KEYSTORE_PATH/private/mfr_root.csr -subj "/C=$MFR_CT/ST=$MFR_ST/L=$MFR_CITY/O=$MFR_NAME/CN=Root Firmware Configuration Certificate Authority"

  # Comment: this certificate expires in ~30 years. In practice, vehicle manufacturers
  # will have to determine how long they want to make their firmware certificates
  # valid for. Generally, certs have much shorter expirations in order to enhance
  # security and lessen the chance that someone duplicates the key through its
  # expected lifetime.

  echo "Signing root Manufacturer key."
  openssl x509 -signkey $KEYSTORE_PATH/private/mfr_root.key -in $KEYSTORE_PATH/private/mfr_root.csr -req -days 10957 -out $KEYSTORE_PATH/public/mfr_root.pem
  ROOT_PRIVATE_KEY_PATH=$KEYSTORE_PATH/private/mfr_root.key
  ROOT_CERT_PATH=$KEYSTORE_PATH/public/mfr_root.pem
elif [ $GEN_ROOT_KEY -eq 0 ] && [ $GEN_YEAR_KEY -eq 1 ]; then 
  # We only need the root key if we are generating a product type key
  openssl rsa -in $ROOT_PRIVATE_KEY_PATH -text -noout > /dev/null 2>&1
  if [ $? -eq 1 ] ; then
    exit_failure "$ROOT_PRIVATE_KEY_PATH is not a valid private key."
  fi
fi

# ---- Generating Intermediate CA ----
if [ $GEN_YEAR_KEY -eq 1 ]; then

  echo "Generating Manufacturer Product Year $YEAR key."
  openssl genrsa -out $KEYSTORE_PATH/private/mfr_$YEAR.key 4096
  openssl rsa -in $KEYSTORE_PATH/private/mfr_$YEAR.key -pubout > $KEYSTORE_PATH/public/mfr_$YEAR.key.pub

  echo "Preparing signing request for $YEAR key."
  openssl req -new -sha256 -key $KEYSTORE_PATH/private/mfr_$YEAR.key -out $KEYSTORE_PATH/private/mfr_$YEAR.csr -subj "/C=$MFR_CT/ST=$MFR_ST/L=$MFR_CITY/O=$MFR_NAME/CN=$YEAR Firmware Configuration Certificate Authority"

  # Comment: again, this cert has a relatively long expiry (10 years).
  echo "Signing model year \"$YEAR\" key with root Manufacturer key"
  openssl x509 -req -in $KEYSTORE_PATH/private/mfr_$YEAR.csr -CA $ROOT_CERT_PATH -CAkey $ROOT_PRIVATE_KEY_PATH -CAcreateserial -out $KEYSTORE_PATH/public/mfr_$YEAR.crt -days 3652 -sha256
  YEAR_PRIVATE_KEY_PATH=$KEYSTORE_PATH/private/mfr_$YEAR.key
  YEAR_CERT_PATH=$KEYSTORE_PATH/public/mfr_$YEAR.crt
elif [ $GEN_YEAR_KEY -eq 0 ] && [ $GEN_MODEL_KEY -eq 1 ]; then
  openssl rsa -in $YEAR_PRIVATE_KEY_PATH -text -noout > /dev/null 2>&1
  if [ $? -eq 1 ] ; then
    exit_failure "$YEAR_PRIVATE_KEY_PATH is not a valid private key."
  fi
fi

# ---- Generating Model Key ----
if [ $GEN_MODEL_KEY -eq 1 ]; then
  echo "Generating Tier 1 Model \"$MODEL\" key"
  openssl genrsa -out $KEYSTORE_PATH/private/mfr_model_$MODEL.key 4096
  openssl rsa -in $KEYSTORE_PATH/private/mfr_model_$MODEL.key -pubout > $KEYSTORE_PATH/public/mfr_model_$MODEL.key.pub

  echo "Preparing signing request for Tier 1 model \"$MODEL\" key."
  openssl req -verbose -new -sha256 -key $KEYSTORE_PATH/private/mfr_model_$MODEL.key -out $KEYSTORE_PATH/private/mfr_model_$MODEL.csr -subj "/C=$MFR_CT/ST=$MFR_ST/L=$MFR_CITY/O=$MFR_NAME/CN=$YEAR Model $MODEL Firmware Certificate Key"


  echo "Signing model \"$MODEL\" key with Manufacturer model year $YEAR key"
  # Again, 10-year validity period
  openssl x509 -req -in $KEYSTORE_PATH/private/mfr_model_$MODEL.csr -CA $YEAR_CERT_PATH -CAkey $YEAR_PRIVATE_KEY_PATH -CAcreateserial -out $KEYSTORE_PATH/public/mfr_model_$MODEL.crt -days 3652 -sha256
fi


