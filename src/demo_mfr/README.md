# Demo Tier 1 Certificate Authority and Firmware Generation
## Important!
If you have not generated a valid Tier 1 firmware image, it may be best to do that before continuing with the Automotive Manufacturer tools.
## Generating Certificate Authority
Before creating a firmware image, you have to create the certificate authority. Running the following script will generate a default certificate authority for a "Demo Manufacturer" located in East Lansing, MI, that has a model year equal to the current year, and a single vehicle model called "Sparty."
```
./mfr_gen_certs.sh
```
To generate a custom certificate authority, use the -mn flag to set the name of the demo manufacturer, -py to set the product year, and -pm to set the product model.

If you already have existing certificates and simply want to make lower-level certificates, use the -kr flag and/or -ky flag to specify the path to the root/intermediate certificates:
```
./mfr_gen_certs.sh -kr keystore/private/mfr_root.key -kp keystore/private/mfr_2019.key -pm "Kart"
```
## Creating a firmware image
To create the manufacturer segment of the firmware image update, use the gen\_fw\_config.sh script, specifying the path to the product model key using the -ks flag, and also specifying the path to the :
```
./gen_fw_config.sh -ks keystore/private/mfr_model_Sparty.key ../demo_tier1/ECU_Computer_001.fw
```
You will be prompted to edit a YAML file; edit the configuration to your liking. After exiting, the YAML file will be converted to Base64, signed, and compiled into a file along with the Tier 1 firmware(default: <year>_Sparty.fw).

If you also need to create an initial image for the demo\_ecu program, you will need to pass a few more arguments to specify the paths to the public signing certificates that will be included in the firmware image:
```
./gen_fw_config.sh -i -ks keystore/private/mfr_model_Sparty.key -km keystore/public/mfr_model_Sparty.crt ../demo_tier1/keystore/public/ECU_Computer_001.crt ../demo_tier1/ECU_Computer_001.fw
```
This will produce a firmware file called demo\_ecu\_fw.conf that can be used to run with the demo ECU node.
