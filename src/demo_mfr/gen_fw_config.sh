#!/bin/sh

KEYSTORE=./keystore
YEAR=$(date +%Y)
MODEL=Sparty
PRIVATE_KEY_PATH=$KEYSTORE/private/mfr_model_$MODEL.key
CONFIG_FILE=
print_usage()
{
  echo "gen_config_update : Generate signed combined firmware and configuration blob"
  echo ""
  echo "Usage: gen_config OPTIONS tier1_update_file"
  echo "  tier1_update_file: File generated by gen_config_firmware in demo_tier1"
  echo "  -c <config> : path to config file (default: prompts for config options)"
  echo "  -ks, --private-key <keyfile> : path to private signing key (default: $KEYSTORE/private/mfr_model_$MODEL.key)"
  echo "  -kt, --public-cert <certfile> : path to public X.509 cert for Tier 1 firmware key"
  echo "  -km, --public-cert <certfile> : path to public X.509 cert for signing key (default: $KEYSTORE/private/mfr_model_$MODEL.crt)"
  echo "  -o <file> : Output file for firmware"
  echo "  -i, --init : also outputs initial firmware file for loading by demo_ecu (default: demo_ecu_fw.conf)"
  echo "  -h, -?, --help: print this help."
  
}

exit_failure()
{
if [ "$1" != "" ]; then
  echo "Error: $1"
else
  echo "Exited with unknown failure."
fi
print_usage
exit
}

INPUT_FIRMWARE_FILE=
OUTPUT_INIT_DEMO_ECU_FW=0
CUSTOM_OUTPUT_FW_FILE=0
OUTPUT_FILE_PATH="${YEAR}_${MODEL}.fw"

X509_MFR=
X509_TIER1= 

while [ "$1" != "" ]; do
  case $1 in
    -h|-\?|--help)
      echo "Help requested."
      print_usage
      exit
      ;;

    -c)
      shift
      if [ -e $1 ]; then CONFIG_FILE=$1; else exit_failure "$1 is not a valid config file."; fi;
      ;;

    -ks)
      shift
      if [ ! -e $1 ]; then exit_failure "-ks requires additional parameter."; fi;
      if [ -e $1 ]; then PRIVATE_KEY_PATH=$1; else exit_failure "$1 is not a valid keyfile."; fi;
    ;;

    -i|--init)
      OUTPUT_INIT_DEMO_ECU_FW=1
    ;;

    -km|--key-manufacturer)
      if [ ! -e "$2" ]; then exit_failure "$1 requires additional parameter."; fi;
      shift
      echo "Setting Mfr keyfile: $1"
      if [ -e $1 ]; then X509_MFR=$1; else exit_failure "$1 is not a valid X509 file."; fi;
    ;;

    -kt|--key-tier1)
      if [ ! -e "$2" ]; then exit_failure "$1 requires additional parameter."; fi;
      shift
      echo "Setting Tier1 keyfile: $1"
      if [ -e $1 ]; then X509_TIER1=$1; else exit_failure "$1 is not a valid X509 file."; fi;
    ;;
    -o)
    if [ -z "$2" ]; then exit_failure "$1 requires additional parameter."; fi
    shift
    CUSTOM_OUTPUT_FW_FILE=1
    OUTPUT_FILE_PATH="$1"
    ;;
    *)
      if [ "$INPUT_FIRMWARE_FILE" != "" ]; then exit_failure "multiple input firmware files defined."; fi;
      if [ ! -e $1 ]; then exit_failure "$1 is not a valid file."; fi;
      INPUT_FIRMWARE_FILE=$1
    ;;
  esac
  shift
done
if [ $CUSTOM_OUTPUT_FW_FILE -eq 0 ]; then
  OUTPUT_FILE_PATH=${YEAR}_${MODEL}.fw
fi;

if [ ! -e "$INPUT_FIRMWARE_FILE" ]; then exit_failure "No firmware file specified."; fi

if [ "$OUTPUT_INIT_DEMO_ECU_FW" -eq 1 ]; then
  if [ ! -e "$X509_MFR" ] ; then exit_failure "-i/--init requires -km argument."; fi;
  if [ ! -e "$X509_TIER1" ] ; then exit_failure "-i/--init requires -kt argument."; fi;
fi

if [ ! -e $INPUT_FIRMWARE_FILE ]; then
  exit_failure "Input firmware file not defined."
fi

if [ ! -e "$CONFIG_FILE" ]; then
  CONFIG_FILE=.tmp_cfg
  echo "message_frequency : 5" > $CONFIG_FILE
  echo "replace_root_certs : false" >> $CONFIG_FILE
  echo "root_certs : []" >> $CONFIG_FILE
  vim $CONFIG_FILE
fi

echo "Converting config to Base64"
CFG_STRING=$(cat $CONFIG_FILE | base64 -w 0)
cp $INPUT_FIRMWARE_FILE .tmp_fw
echo -n $CFG_STRING >> .tmp_fw
echo "Signing firmware with SHA256 ($PRIVATE_KEY_PATH) (len $(wc -c .tmp_fw))"
CFG_SIG=$(openssl dgst -sha256 -sign $PRIVATE_KEY_PATH .tmp_fw | base64 -w 0)
echo "\n$CFG_SIG" >> .tmp_fw
echo "Writing firmware file as $OUTPUT_FILE_PATH"
mv .tmp_fw "$OUTPUT_FILE_PATH";

if [ -e .tmp_cfg ]; then rm .tmp_cfg; fi
if [ -e .tmp_blob ]; then rm .tmp_blob; fi

if [ "$OUTPUT_INIT_DEMO_ECU_FW" -eq 1 ]; then
  if [ ! -e "$X509_MFR" ] ; then exit_failure "-i/--init requires -km argument."; fi;
  if [ ! -e "$X509_TIER1" ] ; then exit_failure "-i/--init requires -kt argument."; fi;
  echo "Outputting startup for demo_ecu to demo_ecu_fw.conf";
  FW_STARTUP_X509_T1=$(cat $X509_TIER1 | base64 -w 0) 
  FW_STARTUP_X509_MF=$(cat $X509_MFR   | base64 -w 0)  
  FW_STARTUP_FW=$(cat $INPUT_FIRMWARE_FILE | head -n 1)
  FW_STARTUP_SIG=$(cat $INPUT_FIRMWARE_FILE | tail -n 1)
  echo $FW_STARTUP_FW > demo_ecu_fw.conf
  echo $FW_STARTUP_SIG >> demo_ecu_fw.conf
  echo $FW_STARTUP_X509_T1 >> demo_ecu_fw.conf
  echo $CFG_STRING >> demo_ecu_fw.conf
  echo $CFG_SIG >> demo_ecu_fw.conf
  echo $FW_STARTUP_X509_MF >> demo_ecu_fw.conf
fi
echo "Done!"

