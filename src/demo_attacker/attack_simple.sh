#!/bin/sh
#
# Simple attack - modify firmware and/or config, and attempt to flash
FIRMWARE_FILE="$1"
cat $FIRMWARE_FILE | head -n 1 | base64 -d > /tmp/fw_tampered
vim /tmp/fw_tampered
echo "1"
cat /tmp/fw_tampered | base64 -w 0 > /tmp/fw_img_tampered
echo -n "\n" >> /tmp/fw_img_tampered
cat $FIRMWARE_FILE | head -n 2 | tail -n 1 >> /tmp/fw_img_tampered
cat $FIRMWARE_FILE | tail -n 2 | head -n 1 | base64 -d > /tmp/cfg_tampered
vim /tmp/cfg_tampered
echo "2"
cat /tmp/cfg_tampered | base64 -w 0 >> /tmp/fw_img_tampered
echo -n "\n" >> /tmp/fw_img_tampered
cat $FIRMWARE_FILE | tail -n 1 >> /tmp/fw_img_tampered

rosrun ecu_encryption demo_updater _fw_file:=/tmp/fw_img_tampered
#rm /tmp/fw_tampered /tmp/cfg_tampered /tmp/fw_img_tampered

