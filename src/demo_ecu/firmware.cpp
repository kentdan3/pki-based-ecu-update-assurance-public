/**
 * The MIT License (MIT)
 * Copyright (c) 2020 Daniel Kent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// This object header
#include "demo_ecu/firmware.h"

// Std Dependencies
#include <ios>
#include <fstream>
#include <vector>
#include <thread>

// Local Dependencies
#include "common/crypto.h"

namespace ECU
{

FirmwareManager::FirmwareManager(ros::NodeHandle & n, ros::NodeHandle & pn)
{
  std::string fw_filename, fw_filename_key;
  bool search_fw_file_param = pn.searchParam("firmware", fw_filename_key);
  if(search_fw_file_param)
  {
    ROS_INFO("Firmware filename key: %s", fw_filename_key.c_str());
  }
  else
  {
    ROS_INFO("No firmware filename key found.");
  }
  bool get_fw_file_param = pn.getParam("firmware", fw_filename);
  if(get_fw_file_param)
  {
    ROS_INFO("Got firmware file: %s", fw_filename.c_str());
  }
  else
  {
    ROS_INFO("Unable to get firmware filename.");
  }
  if(_load_firmware(fw_filename) == false)
  {
    ROS_INFO("Firmware file wasn't valid or not found.");
    ros::shutdown();
  }
  std::string mfr_blob = _fw.firmware_blob+"\n"+_fw.firmware_sig+"\n"
                         +_fw.config_blob;

  if(Crypto::key_signature_check(_fw.firmware_blob, _fw.firmware_sig, _fw.firmware_pubkey) == false or
     Crypto::key_signature_check(mfr_blob, _fw.config_sig, _fw.config_pubkey) == false)
  {
    ROS_INFO("Stored firmware could not be self-validated. Firmware is corrupt or has been tampered with.");
    if(Crypto::key_signature_check(_fw.firmware_blob, _fw.firmware_sig, _fw.firmware_pubkey) == false)
    {
      ROS_INFO("Tier1 firmware not valid.");
    }
    if(Crypto::key_signature_check(mfr_blob, _fw.config_sig, _fw.config_pubkey) == false)
    {
      ROS_INFO("Mfr firmware config not valid.");
    }
    ros::shutdown();
  }
  _update_srv = n.advertiseService("ecu_update", &FirmwareManager::update_firmware, this);
}

double FirmwareManager::get_period()
{
  // Note: we re-validate that the frequency configuration is permissable.
  //   Based on the firmware load check, it should already be in the correct
  //   range, but it could have been tampered with on-device.

  std::lock_guard<std::mutex> lock(_fw_mutex);
  std::string fw_yaml_str = "";
  macaron::Base64::Decode(_fw.firmware_blob, fw_yaml_str);
  std::string cfg_yaml_str = "";
  macaron::Base64::Decode(_fw.config_blob, cfg_yaml_str);

  if(fw_yaml_str.length() == 0 or cfg_yaml_str.length() == 0)
    return 0;
  YAML::Node fw_yaml = YAML::Load(fw_yaml_str);
  YAML::Node cfg_yaml = YAML::Load(cfg_yaml_str);
  double frequency = cfg_yaml["message_frequency"].as<double>();
  double min_frequency = fw_yaml["min_frequency"].as<double>();
  double max_frequency = fw_yaml["max_frequency"].as<double>();
  if(frequency <= 0)
    return 0;
  if(frequency < min_frequency)
    return 1/min_frequency;
  if(frequency > max_frequency)
    return 1/max_frequency;
  return 1/frequency;
}

std::string FirmwareManager::get_message()
{

  std::lock_guard<std::mutex> lock(_fw_mutex);
  std::string fw_yaml = "";
  macaron::Base64::Decode(_fw.firmware_blob, fw_yaml);
  if(fw_yaml.length() == 0)
    return 0;
  YAML::Node yaml = YAML::Load(fw_yaml);
  std::string message = yaml["message_string"].as<std::string>();
  return message;
  
}

bool FirmwareManager::update_firmware(ecu_encryption::EcuUpdate::Request & update_request,
                                 ecu_encryption::EcuUpdate::Response & update_response)
{

  std::lock_guard<std::mutex> fw_lock_guard(_fw_mutex);
  std::cerr << " === Firmware === " << std::endl;
  std::cerr << update_request.firmware_blob << std::endl;
  std::cerr << " === Firmware Sig === " << std::endl;
  std::cerr << update_request.firmware_sig << std::endl;
  std::cerr << " === Config === " << std::endl;
  std::cerr << update_request.config_blob << std::endl;
  std::cerr << " === Config Sig === " << std::endl;
  std::cerr << update_request.config_sig << std::endl;
  std::string mfr_blob = update_request.firmware_blob + "\n" + update_request.firmware_sig + "\n" + update_request.config_blob;
  if(Crypto::key_signature_check(mfr_blob, update_request.config_sig, _fw.config_pubkey) == false)
  {
    update_response.success = false;
    update_response.message = "Configuration signature check failed. Firmware, firmware signature, configuration, or configuration signature are corrupt or have been tampered with.";
    return true;
  }
  if(Crypto::key_signature_check(update_request.firmware_blob, update_request.firmware_sig, _fw.firmware_pubkey) == false)
  {
    update_response.success = false;
    update_response.message = "Firmware signature check failed. Firmware or firmware signature are corrupt or have been tampered with.";
    return true;
  }
  _fw.firmware_blob = update_request.firmware_blob;
  _fw.firmware_sig = update_request.firmware_sig;
  _fw.config_blob = update_request.config_blob;
  _fw.config_sig = update_request.config_sig;
  _save_current_firmware(false);
  update_response.message = "Firmware was updated successfully.";
  update_response.success = true;
  return true;
}

bool FirmwareManager::_load_firmware(const std::string & filename)
{
  ROS_INFO("Loading %s", filename.c_str());
  std::ifstream fw_file(filename, std::ifstream::in | std::ifstream::binary);
  if(fw_file)
  {
    std::string blob;
    fw_file.seekg(0, fw_file.end);
    blob.resize(fw_file.tellg());
    fw_file.seekg(0, fw_file.beg);
    fw_file.read(&blob[0], blob.size());
    fw_file.close();
    std::lock_guard<std::mutex> fw_lock_guard(_fw_mutex);
    _fw.filename = filename;
    _fw = _parse_firmware_string(blob);
    return true;
  }
  else
  {
    return false;
  }
  return false;
}

Firmware FirmwareManager::_parse_firmware_string(const std::string & fw_blob)
{
  Firmware fw;
  std::vector<std::string> firmware_strings;
  size_t last = 0, next = 0;
  while((next = fw_blob.find("\n", last)) != std::string::npos)
  {
    firmware_strings.push_back(fw_blob.substr(last, next-last));
    last = next+1;
  }
  firmware_strings.push_back(fw_blob.substr(last, fw_blob.size()-last));
  /*
  for(const std::string fw_string : firmware_strings)
    std::cerr << fw_string << " ";
  std::cerr << std::endl;
  */
  if(firmware_strings.size() < 6)
    return fw;
  if(!Crypto::key_signature_check(firmware_strings[0], firmware_strings[1], firmware_strings[2])
     or !Crypto::key_signature_check(firmware_strings[0] + '\n' + firmware_strings[1] + '\n' + firmware_strings[3], firmware_strings[4], firmware_strings[5])) 
  {
    return fw;
  }
  fw.firmware_blob = firmware_strings[0];
  fw.firmware_sig = firmware_strings[1];
  fw.firmware_pubkey = firmware_strings[2];

  fw.config_blob = firmware_strings[3];
  fw.config_sig = firmware_strings[4];
  fw.config_pubkey = firmware_strings[5];
  return fw;
}

bool FirmwareManager::_save_firmware(const Firmware & fw)
{
  std::ofstream firmware_file(_fw.filename, std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);
  if(!firmware_file)
  {
    return false;
  }
  firmware_file << fw.firmware_blob << "\n" << fw.firmware_sig << "\n"
                << fw.firmware_pubkey << "\n" << fw.config_blob << "\n"
                << fw.config_sig << "\n" << fw.config_pubkey;
  firmware_file.close();
  return true;
}

bool FirmwareManager::_save_current_firmware(bool require_lock)
{
  std::shared_ptr<std::lock_guard<std::mutex> > lock = nullptr;
  if(require_lock)
  {
    lock = std::make_shared<std::lock_guard<std::mutex> >(_fw_mutex);
  }
  bool save_result = _save_firmware(_fw);
  return save_result;
}

} // namespace ECU
