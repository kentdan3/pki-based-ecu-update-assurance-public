/**
 * The MIT License (MIT)
 * Copyright (c) 2020 Daniel Kent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "demo_ecu/demo_ecu_node.h"
#include <std_msgs/String.h>

namespace ECU
{

ECU::ECU(ros::NodeHandle &n, ros::NodeHandle &pn) :
  _fwm(n, pn)
{
  ROS_INFO("period: %f", _fwm.get_period());
  _tmr = n.createTimer(ros::Duration(_fwm.get_period()), &ECU::timer_callback, this, true);
  _pub = n.advertise<std_msgs::String>("ecu_message", 10);
  _tmr.start();
}

ECU::~ECU()
{
  ROS_INFO("Deleting ECU object...");
}

void ECU::timer_callback(const ros::TimerEvent & te)
{
  std_msgs::String ecu_message;
  ecu_message.data = _fwm.get_message();
  _pub.publish(ecu_message);
  _tmr.stop();
  _tmr.setPeriod(ros::Duration(_fwm.get_period()))  ;
  _tmr.start();
}

} // namespace ECU
