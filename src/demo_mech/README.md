# Demo Mechanic's Tools
This utility is used to flash a firmware image while the demo ECU is running. This repository comes with four sample firmware images, which can be flashed using the provided launch file:
```
roslaunch ecu_encryption flash_fw.launch select:=1
roslaunch ecu_encryption flash_fw.launch select:=2
roslaunch ecu_encryption flash_fw.launch select:=3
roslaunch ecu_encryption flash_fw.launch select:=4
```
The first three updates are valid updates for the provided default firmware image, and the fourth is an invalid image that has been "tampered" with.

## Using Your Own Firmware Update
To use your own firmware image, either call the updater directly, or use the launch fil with the "select" parameter set to 0, and the fw_custom parameter set to the path of the update file:
```
rosrun ecu_encryption demo_updater _fw_file:=/path/to/file.fw
roslaunch ecu_encryption flash_fw.launch select:=0 fw_custom:=/path/to/file.fw
``` 
