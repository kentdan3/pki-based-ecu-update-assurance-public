#include <ros/ros.h>
#include <ios>
#include <fstream>
#include "ecu_encryption/EcuUpdate.h"

void sigint_handler(int signal)
{
  ros::shutdown();
}

int main(int argc, char * argv[])
{
  ros::init(argc, argv, "demo_ecu_flasher", ros::init_options::NoSigintHandler);
  ros::NodeHandle n;
  ros::NodeHandle pn("~");
  std::string fw_file;
  if(pn.getParam("fw_file", fw_file) == false)
  {
    ROS_ERROR("No firmware file provided.");
    return 1;
  }
  
  ROS_INFO("Loading %s", fw_file.c_str());
  std::ifstream fw_fs(fw_file.c_str(), std::ifstream::in | std::ifstream::binary);
  std::string fw_blob;
  if(fw_fs)
  {
    fw_fs.seekg(0, fw_fs.end);
    fw_blob.resize(fw_fs.tellg());
    fw_fs.seekg(0, fw_fs.beg);
    fw_fs.read(&fw_blob[0], fw_blob.size());
    fw_fs.close();
  }
  else
  {
    ROS_ERROR("Could not parse firmware file. Does file exist?");
    return 2;
  }
  std::vector<std::string> firmware_strings;
  size_t last = 0, next = 0;
  while((next = fw_blob.find("\n", last)) != std::string::npos)
  {
    firmware_strings.push_back(fw_blob.substr(last, next-last));
    last = next+1;
  }
  firmware_strings.push_back(fw_blob.substr(last, fw_blob.size()-last));
  if(firmware_strings.size() < 4)
  {
    ROS_ERROR("Firmware file does not contain all four components.");
    return 3;
  }

  // Note: it would be a good idea in practice for the flasher utility to
  // go ahead and validate the firmware image before flashing at this point.
  // We're going to skip this for this demo code.

  ros::ServiceClient client = n.serviceClient<ecu_encryption::EcuUpdate>("/ecu_update");
  ecu_encryption::EcuUpdate update;
  update.request.firmware_blob = firmware_strings[0];
  update.request.firmware_sig = firmware_strings[1];
  update.request.config_blob = firmware_strings[2];
  update.request.config_sig = firmware_strings[3];
  if(client.call(update))
  {
    if(update.response.success)
    {
      ROS_INFO("%s", update.response.message.c_str());
      return 0;
    }
    else
    {
      ROS_ERROR("%s", update.response.message.c_str());
      return 4;
    }
  }
  else
  {
    ROS_ERROR("Update service did not return anything. Unknown if update occured.");
    return 5;
  }
  // How did I... Get here?
  return 6;
}
