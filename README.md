# ECU Split-Key Update Demonstration

A demonstration implementation of a split-key firmware/configuration scheme for
assuring ECU firmware integrity and ECU firmware update integrity. Originally 
developed by Daniel Kent at Michigan State University, Spring 2019. Submitted
to escar 2020

# Read The Paper!
The final version of this paper will be added to this repository after publication.

# System Requirements

This code was developed to run on
[Ubuntu 18.04](http://releases.ubuntu.com/18.04/) with the Robot Operating
System (ROS) middleware, version 12 (Melodic).
[Installation instructions for installing ROS Kinetic on Ubuntu 16.04 can be
found here.](http://wiki.ros.org/kinetic/Installation/Ubuntu). In addition to
ROS, the target system should also have OpenSSL, yaml-cpp, and Botan2 installed.
Debian-based systems should already have a sufficient version of OpenSSL
installed; to install Botan2 and yaml-cpp, run:

```
sudo apt install libbotan-2-dev libyaml-cpp-dev
```

This code also uses vim as the default editor for some scripts; to install on Debian-based systems:
```
sudo apt install vim
```

This code should operate on any combination of Linux with the above dependencies
installed, but other target configurations have not been tested. Note that
Ubuntu only includes Botan2 in 18.04 and above, so running this code on versions
earlier than 16.04 will require installing Botan2 from source. The ROS portions
of the code should work on older versions of ROS, but these have not been
tested.

# Usage Instructions
## Initializing Catkin Workspace
After installing Ubuntu 18.04 with ROS Melodic and all the other dependencies,
either create a catkin workspace or add the source code in this repository to an
existing catkin workspace source directory.
### New Catkin Workspace
```
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
catkin_init_workspace
cp -r /path/to/demo_ecu_update_simulator .
cd ../
```
### Existing Catkin Workspace
```
cd ~/your_existing_catkin_ws/src
cp -r /path/to/demo_ecu_update_simulator .
cd ../
```
## Install Dependencies
Verify that all dependencies for the code have been installed:
```
rosdep install --from-paths src --ignore-src
```
## Compile Code
Compile the code:
```
catkin_make -j4
```
## Quick Start
This repository comes with two valid firmware files and a pre-initialized ECU firmware file. To run the ECU:
```
roslaunch ecu_encryption demo_ecu.launch
```

A demo ECU node should come online, publishing the /ecu_message topic. By default, it should publish the message "ECU firmware is working normally." You can verify this by running the following commands for message contents and message rate respectively:
```
rostopic echo /ecu_message
rostopic hz /ecu_message
```
To flash either alternate firmware image, run one of the following commands:
```
roslaunch ecu_encryption flash_fw.launch select:=2
roslaunch ecu_encryption flash_fw.launch select:=3
```

To attempt to flash an invalid firmware image, run the following command:
```
roslaunch ecu_encryption flash_fw.launch select:=4
```

To flash back to the original firmware image, run the following command:
```
roslaunch ecu_encryption select:=1
```

## Generating and Flashing Your Own Firmware
To generate your own firmware images, you need to first create the key structure
for each of the demonstration Tier 1 and Manufacturers, then use these keys to
create the firmware. These scripts are documented in their respective directories:

[Demo Tier 1 Documentation](src/demo_tier1/README.md)

[Demo Manufacturer Documentation](src/demo_mfr/README.md)

Then, use the [Demo Mechanic Tools](src/demo_mech/README.md)

## Tampering with Firmware Files
Some additional tools have been developed to help attempts to tamper with the firmware. To attempt a firmware modification type attack, run the basic attack tool:
```
roslaunch ecu_encryption attack_basic.launch
```
To attempt the on-ECU firmware modification attack, run the following:
```
roslaunch ecu_encryption attack_stored.launch
```

# SECURITY WARNING
This code is meant to demonstrate the core split-key update concept. The code is not in and of itself guaranteed to be secure. Several possible intrinsic security flaws that exist in this current implementation that were not addressed:

1. Utilization of ROS messages/services means that MITM attacks are trivial.
2. The demo ECU code always allows for firmware/configuration updates. Real-
world systems should not always allow for firmware updates, or else infinite
update loops could effectively act as Denial of Service attacks.
3. String and other input validation beyond signature verification is minimal,
at best. 
4. The entire crypto system has not been end-to-end independently validated.

# Acknowledgements
Megumi Tomita wrote the [base64 implementation used in this project](https://gist.github.com/tomykaira/f0fd86b6c73063283afe550bc5d77594).

# License
[All code in this repository is licensed under the MIT license](LICENSE)
