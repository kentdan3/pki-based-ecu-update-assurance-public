/**
 * The MIT License (MIT)
 * Copyright (c) 2020 Daniel Kent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef ECU_COMMON_CRYPTO_H
#define ECU_COMMON_CRYPTO_H

//#define SIGNATURE_DEBUG 1

// System-level Dependencies
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>

// Botan2
#include <botan/pem.h>
#include <botan/x509cert.h>
#include <botan/secmem.h>
#include <botan/data_src.h>
#include <botan/b64_filt.h>
#include <botan/pipe.h>
#include <botan/pubkey.h>

// Std Dependencies
#include <sstream>
#include <iostream>
#include <string>

// Local Dependencies
#include "base64.h"

namespace Crypto
{

void signature_debug(const std::string & msg)
{
  #ifdef SIGNATURE_DEBUG
  std::cerr << msg << std::endl;
  #endif
  return;
}

bool key_signature_check(std::string blob, std::string sig, std::string key)
{
  Botan::Pipe pipe(new Botan::Base64_Decoder);
  pipe.process_msg(key);
  std::string decoded_cert = pipe.read_all_as_string(0);
  if(decoded_cert == "")
  {
    signature_debug("Key isn't in Base64.");
    return false;
  }
  std::string label = "";
  Botan::DataSource_Memory dsm(decoded_cert);
  signature_debug("Loaded key into Botan memory");
  Botan::X509_Certificate cert(dsm);
  signature_debug("Botan decoded X.509 Cert");
  std::unique_ptr<Botan::Public_Key> pubkey = cert.load_subject_public_key();
  Botan::PK_Verifier verifier(*pubkey, "EMSA3(SHA-256)");
  pipe.process_msg(sig);
  Botan::secure_vector<uint8_t> sig_data = pipe.read_all(1);
  signature_debug("Blob length: " + blob.size());
  signature_debug("Sig length: " + sig_data.size());

  verifier.update(blob.data());
  if(verifier.check_signature(sig_data))
  {
    return true;
  }
  else
  {
    return false;
  }
  
  return false;
}

}

#endif // ECU_COMMON_CRYPTO_H
