/**
 * The MIT License (MIT)
 * Copyright (c) 2020 Daniel Kent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef ECU_FIRMWARE_H
#define ECU_FIRMWARE_H

// ROS Dependencies
#include <ros/ros.h>

// System Dependencies
#include <yaml-cpp/yaml.h>

// Std Dependencies
#include <mutex>


// Local Dependencies
#include "ecu_encryption/EcuUpdate.h"

/*
 *    FORMAT FOR FIRMWARE FILES
 *    ==========================
 * Firmware files are separated into six lines, as follows:
 *
 * 1. Base64-encoded firmware file (for this demo ECU, this is a YAML file)
 * 2. Base64-encoded firmware signature
 * 3. Base64-encoded DER Tier1 cert (essentially the Base64 part of a PEM cert)
 * 4. Base64-encoded configuration file (Again, just a YAML file)
 * 5. Base64-encoded firmware+firmware sig+configuration signature (all combined
 *    into one string, separated by a \n character
 * 6. Base64-encoded DER Manufacturer cert (just the Base64 part of a PEM cert)
 */

namespace ECU
{

struct Firmware
{
  std::string filename;
  
  std::string firmware_blob;
  std::string firmware_sig;
  std::string firmware_pubkey;

  std::string config_blob;
  std::string config_sig;
  std::string config_pubkey;
};

class FirmwareManager
{
public:
  FirmwareManager() = delete;
  FirmwareManager(ros::NodeHandle & n, ros::NodeHandle & pn);
  std::string get_message();
  double get_period();
  bool update_firmware(ecu_encryption::EcuUpdate::Request & update_request,
                       ecu_encryption::EcuUpdate::Response & update_response);
  
private:
  bool _validate_signature(const std::string & blob, const std::string & sig, const std::string & key);
  bool _load_firmware(const std::string & fw_filename);
  bool _save_current_firmware(bool require_lock = true);
  bool _save_firmware(const Firmware & fw);
  Firmware _parse_firmware_string(const std::string & fw_blob);
  ros::ServiceServer _update_srv;
  Firmware _fw;
  std::mutex _fw_mutex;
};

} // namespace ECU

#endif // ECU_FIRMWARE_H
