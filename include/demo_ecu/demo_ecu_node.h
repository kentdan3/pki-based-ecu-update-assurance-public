/**
 * The MIT License (MIT)
 * Copyright (c) 2020 Daniel Kent
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef DEMO_ECU_NODE_H
#define DEMO_ECU_NODE_H

// ROS Dependencies
#include <ros/ros.h>

// Local Dependencies
#include "demo_ecu/firmware.h"

namespace ECU
{

class ECU
{
public:
  ECU() = delete;
  ECU(ros::NodeHandle & n, ros::NodeHandle & pn);
  ~ECU();

  void timer_callback(const ros::TimerEvent & te);
private:
  FirmwareManager _fwm;
  ros::Timer _tmr;
  ros::Publisher _pub;
};

} // namespace ECU

#endif // DEMO_ECU_NODE_H
