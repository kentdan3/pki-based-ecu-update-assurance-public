####
# The MIT License (MIT)
# Copyright (c) 2020 Daniel Kent
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

# Formatting of Firmware/Config
# Firmware is a Base64 string containing a YAML configuration
# with the following format:
# message_string: some message string here
# min_frequency: 1 % minimum frequency
# max_frequency: 10 % maximum frequency
#
# Config is a Base64 string containing a YAML configuration
# with the following format:
# message_frequency: 10
#
# where the number is in hertz.
#
# Signature for the firmware blob is the sha256sum of the firmware blob base64 string
# (null-terminated) encrypted by the lowest-level tier 1 key (product key in the Kent
#  et al proposal), encoded in Base64
#
# Signature for the configuration is the sha256sum of the firmware blob base64 string,
# plus a newline character (\n), plus the base64 encoding of the firmware signature,
# plus a newline character (\n), plus the base64 encoding of the config blob; this
# string is then encrypted by the lowest level manufacturer key (model key in the Kent
#  et al proposal), encoded in Base64

string firmware_blob
string firmware_sig
string config_blob
string config_sig
---
bool success
string message
